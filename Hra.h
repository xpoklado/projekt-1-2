#ifndef HRA_H
#define HRA_H
#include <iostream>
#include <vector>
using namespace std;
using namespace Zmena_polohy;


namespace Engine {
	class Hra {

	private:
		vector<Zmena_polohy::Poloha*> m_zmenaPolohy;
		Postava::Hrac* m_hrac;
		vector<Vysledek*> m_vysledky;

	public:
		Hra();
		void VypisRejestrik();
		void ZvolitSmer();
		void VytvorPostavu();
		void VytvorMenu();
		void ulozVysldek(Engine::Vysledek* vysledek);
		~Hra();
	};
}
#endif // HRA_H
