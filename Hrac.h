#ifndef HRAC_H
#define HRAC_H
#include <iostream>
using namespace std;
using namespace Postava;


namespace Postava {
	class Hrac {

	private:
		string m_jmeno;
		int m_zdravi;
		int m_sila;
		int m_penize;
		Postava::Inventar* m_inventar;

	public:
		Hrac(string jmeno);
		string getJmeno();
		int getZdravi();
		int getSila();
		int getPenize();
		Postava::Inventar* getInventar();
		void setJmeno(string jmeno);
		void pridejPenize(int cenaZaProdej);
		void odeberPenize(int cenaZaNakup);
		void printInfo();
	};
}
#endif // HRAC_H
