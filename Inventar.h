#ifndef INVENTAR_H
#define INVENTAR_H
#include <iostream>
#include <vector>
using namespace std;

namespace Postava {
	class Inventar {

	private:
		vector<Zbozi*> m_zbozi;
		int m_kapacita;
		int m_pocetZbozi;
		int m_pocetZlata;
		int m_pocetZeleza;
		int m_pocetUhli;
		int m_pocetChlebu;
		int m_pocetBoruvek;
		int m_pocetZeleniny;

	public:
		Inventar(int kapacita);
		int getKapacita();
		int getPocetZbozi();
		int getPocetZlata();
		int getPocetZeleza();
		int getPocetUhli();
		int getPocetChlebu();
		int getPocetBoruvek();
		int getPocetZeleniny();
		void pridejZbozi(Zbozi* zbozi);
		void odeberZbozi(Zbozi* zbozi);
		void printInfo();
	};
}
#endif // INVENTAR_H
