#ifndef LES_H
#define LES_H
#include <iostream>
using namespace std;
using namespace Postava;


namespace Zmena_polohy {
	class Les :public Zmena_polohy::Poloha {

	private:
		Loupeznik* m_loupeznik;
		Postava::Hrac* m_hrac;

	public:
		Les(string nazevPolohy);
		string getNazev();
	};
}
#endif // LES_H
