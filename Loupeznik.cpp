#include "Loupeznik.h"

Loupeznik::Loupeznik(string jmeno, int sila) {
	m_jmeno = jmeno;
	m_sila = sila;
}

void Loupeznik::Napadni(Postava::Hrac* hrac) {
	if ((Postava::Hrac->getSila())< m_sila){
        Postava::Hrac->getZdravi() -= 100;
        Postava::Hrac->getSila() += 5;
	};
}

int Loupeznik::getSila() {
	return m_sila;
}
