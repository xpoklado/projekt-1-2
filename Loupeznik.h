#ifndef SKALA_H
#define SKALA_H
#include <iostream>
using namespace std;

class Loupeznik {

private:
	string m_jmeno;
	int m_sila;

public:
	Loupeznik(string jmeno, int sila);
	void Napadni(Postava::Hrac* hrac);
	int getSila();
};
