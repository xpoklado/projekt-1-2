#ifndef MESTO_H
#define MESTO_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class Mesto :public Zmena_polohy::Poloha {

	private:
		Obchod* m_obchod;
		Zbozi* m_zbozi;
		Postava::Hrac* m_hrac;

	public:
		Mesto(string nazevPolohy);
		string getNazev();
	};
}
#endif // MESTO_H
