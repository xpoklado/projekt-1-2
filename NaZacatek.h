#ifndef NAZACATEK_H
#define NAZACATEK_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class NaZacatek :public Zmena_polohy::Poloha {

	public:
		NaZacatek(string nazev);
		string getNazev();
		void printInfo();
	};
}
#endif // NAZACATEK_H
