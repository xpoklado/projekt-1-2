#ifndef OBCHOD_H
#define OBCHOD_H
#include <iostream>
#include<vector>
using namespace std;

class Obchod {

private:
	vector<Zbozi*> m_zbozi;
	int m_cenaZaProdej;
	int m_cenaZaNakup;
	int m_pocetZbozi;

public:
	void prodejZbozi(int cenaZaNakup);
	void nakupZbozi(int cenaZaProdej);
	int getPocetZbozi();
	void printInfo();
};
#endif // OBCHOD_H
