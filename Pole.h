#ifndefine POLE_H
#define POLE_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class Pole :public Zmena_polohy::Poloha {

	public:
		Postava::Hrac* m_hrac;
		Pole(string nazevPolohy);
		string getNazev();
		void printInfo();
	};
}
#endif // POLE_H
