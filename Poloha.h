#ifndef POLOHA_H
#define POLOHA_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class Poloha {

	private:
		string m_nazevPolohy;
		Postava::Hrac* m_hrac;

	public:
		Poloha(string nazev);
		virtual string getNazev() = 0;
		virtual Postava::Hrac* ZmenPolohu() = 0;
		virtual void printInfo() = 0;
	};
}
#endif // POLOHA_H
