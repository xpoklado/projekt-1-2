#ifndef SKALA_H
#define SKALA_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class Skala : Zmena_polohy::Poloha {

	public:
		Postava::Hrac* m_hrac;
		Skala(string nazevPolohy);
		Postava::Hrac* ZmenPolohu();
		string getNazev();
		void printInfo();
	};
}
#endif // SKALA_H
