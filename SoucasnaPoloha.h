#ifndef SOUCASNAPOLOHA_H
#define SOUCASNAPOLOHA_H
#include <iostream>
using namespace std;
using namespace Postava;

namespace Zmena_polohy {
	class SoucasnaPoloha : Zmena_polohy::Poloha {

	public:
		Postava::Hrac* m_hrac;
		SoucasnaPoloha(string nazev);
		Postava::Hrac* zmenPolohu();
		string getNazev();
		void printInfo();
	};
}
#endif // SOUCASNAPOLOHA_H
