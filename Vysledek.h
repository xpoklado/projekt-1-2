#ifndef VYSLEDEK_H
#define VYSLEDEK_H
#include <iostream>
#include <fstream>
using namespace std;


namespace Engine {
	class Vysledek {

	private:
		string m_jmenoHrace;
		int m_skoreHrace;

	public:
		Vysledek();
		void UlozVysledek();
	};
}
#endif // VYSLEDEK_H
