#include "Zbozi.h"
using namespace std;

Zbozi::Zbozi(string nazev, int cenaZaNakup, int cenaZaProdej) {
	m_nazevPredmetu = nazev;
	m_cenaZaNakup = cenaZaNakup;
	m_cenaZaProdej = cenaZaProdej;
}

string Zbozi::getNazev() {
	return m_nazevPredmetu;
}

int Zbozi::getCenaZaNakup() {
	return m_cenaZaNakup;
}

int Zbozi::getCenaZaProdej() {
	return m_cenaZaProdej;
}

void Zbozi::printInfo() {
	cout<<"Zbozi "<<m_nazevPredmetu<<" muzete prodat za "<<m_cenaZaProdej<<endl;
}
