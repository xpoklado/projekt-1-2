#ifndef ZBOZI_H
#define ZBOZI_H
#include <iostream>
#include <vector>
using namespace std;

class Zbozi {

private:
	string m_nazevPredmetu;
	int m_cenaZaNakup;
	int m_cenaZaProdej;

public:
	Zbozi(string nazev, int cenaZaNakup, int cenaZaProdej);
	string getNazev();
	int getCenaZaNakup();
	int getCenaZaProdej();
	void printInfo();
};
#endif // ZBOZI_H
